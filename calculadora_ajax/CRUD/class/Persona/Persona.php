<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registro de Persona</title>
</head>

    <LINK REL=StyleSheet HREF="../../css/style.css" TYPE="text/css" MEDIA=screen>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="ajax_p.js"></script>
<body>

<div>
<table width="500" border="0" align="center">
  <tr>
    <td colspan="4"><h2>Registro de Persona</h2></td>
  </tr>
  
  <tr>
    <td colspan="2">Nombre</td>
    <td colspan="2"><input type="text" name="nombre" id="nombre"/></td>
  </tr>
   <tr>
    <td colspan="2">Identificacion</td>
    <td colspan="2"><input type="text" name="identificacion" id="identificacion"/></td>
  </tr>
  <tr>
    <td colspan="2">Direccion</td>
    <td colspan="2"><input type="text" name="direccion" id="direccion"/></td>
  </tr>
   <tr>
    <td colspan="2">Telefono</td>
    <td colspan="2"><input type="text" name="telefono" id="telefono"/></td>
  </tr>
   <tr>
    <td colspan="4"><p></p></td>
  </tr>
  <tr>
    <td align="center">
    <button id="insertar">Insertar</button>
    </td>
    <td align="center">
    <button id="buscar">Buscar</button>
    </td>
    <td align="center">
    <button id="actualizar">Actualizar</button>
    </td>
    <td align="center">
    <button id="eliminar">Eliminar</button>
    </td>
  </tr>
  <tr>
    <td colspan="4"><p></p></td>
  </tr>
   <tr>
    <td colspan="4"><div id="resultado"></div></td>
  </tr>
</table>

<p>Para eliminar y actualizar peronas debe realizar primero la busqueda, ya que requerimos el numero de identificacion de la persona, la busqueda puede realizarse por nombre o identificacion.</p> 

</div>

</body>
</html>