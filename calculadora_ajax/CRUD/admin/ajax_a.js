jQuery(document).ready(function($){

    $("a").click(function(event)
        {
            link=$(this).attr("href");
            $.ajax(
                {
                    url: link,
                })
            .done(function(html){
                $("#content").empty().append(html);
            })
            .fail(function(html){
                console.log('error');
            })
            .always(function(html){
                console.log('complete');
            });
            return false;
        });

});